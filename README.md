# Library for KiCad by Ravani 
[![NPM](https://img.shields.io/npm/l/react)](https://gitlab.com/VictorRavani/kicad_library_ravani/-/blob/main/LICENSE) 

# About the Repository

This repository is aimed at hardware developers using KiCad software. It contains footprints, symbols and 3D files. The goal is to facilitate the portability of component libraries. To start using it, do a "git clone" to the C:// directory on your computer and use the available components.

# Author

Victor Ravani

https://www.linkedin.com/in/victor-r-65491417a/

