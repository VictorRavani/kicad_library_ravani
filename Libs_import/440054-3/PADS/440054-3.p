*PADS-LIBRARY-PART-TYPES-V9*

440054-3 SHDR3W50P0X200_1X3_800X470X635P I CON 7 1 0 0 0
TIMESTAMP 2023.05.05.19.16.02
"Mouser Part Number" 571-440054-3
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/TE-Connectivity/440054-3?qs=JFPmFzpscO5ATC%252BmN3ka1Q%3D%3D
"Manufacturer_Name" TE Connectivity
"Manufacturer_Part_Number" 440054-3
"Description" AMP - TE CONNECTIVITY - 440054-3 . - CONNECTOR, HEADER, 3POS, 1ROW, 2MM
"Datasheet Link" https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=440054&DocType=Customer+Drawing&DocLang=English&DocFormat=pdf&PartCntxt=440054-3
"Geometry.Height" 6.35mm
GATE 1 3 0
440054-3
1 0 U 1
2 0 U 2
3 0 U 3

*END*
*REMARK* SamacSys ECAD Model
1380812/1129843/2.50/3/3/Connector
