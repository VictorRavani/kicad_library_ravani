PULSONIX_LIBRARY_ASCII "SamacSys ECAD Model"
//12785520/1129843/2.50/5/2/Connector

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "c120_h80"
		(holeDiam 0.8)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.2) (shapeHeight 1.2))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.2) (shapeHeight 1.2))
	)
	(padStyleDef "s120_h80"
		(holeDiam 0.8)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 1.2) (shapeHeight 1.2))
		(padShape (layerNumRef 16) (padShapeType Rect)  (shapeWidth 1.2) (shapeHeight 1.2))
	)
	(textStyleDef "Normal"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 1.27)
			(strokeWidth 0.127)
		)
	)
	(patternDef "SHDR5W60P0X200_1X5_1200X470X63" (originalName "SHDR5W60P0X200_1X5_1200X470X63")
		(multiLayer
			(pad (padNum 1) (padStyleRef s120_h80) (pt 0, 0) (rotation 90))
			(pad (padNum 2) (padStyleRef c120_h80) (pt -2, 0) (rotation 90))
			(pad (padNum 3) (padStyleRef c120_h80) (pt -4, 0) (rotation 90))
			(pad (padNum 4) (padStyleRef c120_h80) (pt -6, 0) (rotation 90))
			(pad (padNum 5) (padStyleRef c120_h80) (pt -8, 0) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 0, 0) (textStyleRef "Normal") (isVisible True))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 2.4 -2.15) (pt -10.4 -2.15) (width 0.05))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -10.4 -2.15) (pt -10.4 3.35) (width 0.05))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -10.4 3.35) (pt 2.4 3.35) (width 0.05))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 2.4 3.35) (pt 2.4 -2.15) (width 0.05))
		)
		(layerContents (layerNumRef 28)
			(line (pt 2.15 -1.9) (pt -10.15 -1.9) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt -10.15 -1.9) (pt -10.15 3.1) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt -10.15 3.1) (pt 2.15 3.1) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 2.15 3.1) (pt 2.15 -1.9) (width 0.025))
		)
		(layerContents (layerNumRef 18)
			(line (pt 0 -1.9) (pt -10.15 -1.9) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -10.15 -1.9) (pt -10.15 3.1) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -10.15 3.1) (pt 2.15 3.1) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 2.15 3.1) (pt 2.15 0) (width 0.2))
		)
	)
	(symbolDef "440054-5" (originalName "440054-5")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -25 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 2) (pt 0 mils -100 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -125 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 3) (pt 0 mils -200 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -225 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 4) (pt 0 mils -300 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -325 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 5) (pt 0 mils -400 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -425 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(line (pt 200 mils 100 mils) (pt 600 mils 100 mils) (width 6 mils))
		(line (pt 600 mils 100 mils) (pt 600 mils -500 mils) (width 6 mils))
		(line (pt 600 mils -500 mils) (pt 200 mils -500 mils) (width 6 mils))
		(line (pt 200 mils -500 mils) (pt 200 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 650 mils 300 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))
		(attr "Type" "Type" (pt 650 mils 200 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))

	)
	(compDef "440054-5" (originalName "440054-5") (compHeader (numPins 5) (numParts 1) (refDesPrefix J)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "3" (pinName "3") (partNum 1) (symPinNum 3) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "4" (pinName "4") (partNum 1) (symPinNum 4) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "5" (pinName "5") (partNum 1) (symPinNum 5) (gateEq 0) (pinEq 0) (pinType Unknown))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "440054-5"))
		(attachedPattern (patternNum 1) (patternName "SHDR5W60P0X200_1X5_1200X470X63")
			(numPads 5)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
				(padNum 3) (compPinRef "3")
				(padNum 4) (compPinRef "4")
				(padNum 5) (compPinRef "5")
			)
		)
		(attr "Mouser Part Number" "571-440054-5")
		(attr "Mouser Price/Stock" "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/440054-5?qs=JFPmFzpscO49oL3wfLTOTw%3D%3D")
		(attr "Manufacturer_Name" "TE Connectivity")
		(attr "Manufacturer_Part_Number" "440054-5")
		(attr "Description" "2.0MM,HDR,5POS,VERTICAL")
		(attr "<Hyperlink>" "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F440054%7FF8%7Fpdf%7FEnglish%7FENG_CD_440054_F8.pdf%7F440054-5")
		(attr "<Component Height>" "6.35")
		(attr "<STEP Filename>" "440054-5.stp")
		(attr "<STEP Offsets>" "X=-4;Y=0;Z=6.05")
		(attr "<STEP Rotation>" "X=0;Y=0;Z=0")
	)

)
