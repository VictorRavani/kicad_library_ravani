*PADS-LIBRARY-PART-TYPES-V9*

440054-2 SHDR2W70P0X200_1X2_600X470X635P I CON 7 1 0 0 0
TIMESTAMP 2023.06.21.14.05.07
"Mouser Part Number" 571-440054-2
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/TE-Connectivity/440054-2?qs=JFPmFzpscO4RQaczYEA68w%3D%3D
"Manufacturer_Name" TE Connectivity
"Manufacturer_Part_Number" 440054-2
"Description" Header vert 2 way 2mm HPI TE Connectivity HPI Series, 2mm Pitch 2 Way 1 Row Straight PCB Header, Solder Termination, 3A
"Datasheet Link" https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F440054%7FF8%7Fpdf%7FEnglish%7FENG_CD_440054_F8.pdf%7F1-440054-2
"Geometry.Height" 6.35mm
GATE 1 2 0
440054-2
1 0 U 1
2 0 U 2

*END*
*REMARK* SamacSys ECAD Model
273704/1129843/2.50/2/3/Connector
