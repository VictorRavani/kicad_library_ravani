(part "171856-0004"
    (packageRef "SHDR4W99P0X254_1X4_1001X635X1105P")
    (interface
        (port "1" (symbPinId 1) (portName "1") (portType INOUT))
        (port "2" (symbPinId 2) (portName "2") (portType INOUT))
        (port "3" (symbPinId 3) (portName "3") (portType INOUT))
        (port "4" (symbPinId 4) (portName "4") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "181")
    (property "Mouser_Part_Number" "538-171856-0004")
    (property "Mouser_Price/Stock" "https://www.mouser.co.uk/ProductDetail/Molex/171856-0004?qs=s7UCm7gO1bZN34vfhOPY%2Fg%3D%3D")
    (property "Manufacturer_Name" "Molex")
    (property "Manufacturer_Part_Number" "171856-0004")
    (property "Description" "Headers & Wire Housings KK Hdr Vrt 4 Ckt 2.54mm Pitch Sn")
    (property "Datasheet_Link" "https://www.molex.com/pdm_docs/sd/1718560004_sd.pdf")
    (property "symbolName1" "171856-0004")
)
