*PADS-LIBRARY-PCB-DECALS-V9*

HC49SSDLF800MHz M 0 0 0 2 4 0 2 1 0
TIMESTAMP 2021.05.01.12.23.32
0.000 0.000 0 0 1.27 0.127 1 0 34 "Regular "
REF-DES
0.000 0.000 0 0 1.27 0.127 1 32 35 "Regular "
PART-TYPE
CLOSED 5 0.1 27 -1
-5.850 2.500
5.850 2.500
5.850 -2.500
-5.850 -2.500
-5.850 2.500
CLOSED 5 0.1 20 -1
-8.500 3.500
8.500 3.500
8.500 -3.500
-8.500 -3.500
-8.500 3.500
OPEN 2 0.2 26 -1
-5.850 2.500
5.850 2.500
OPEN 2 0.2 26 -1
-5.850 -2.500
5.850 -2.500
T-4.750 0.000 -4.750 0.000 1
T4.750 0.000 4.750 0.000 2
PAD 0 3 N 0
-2 2.000 RF 0.000 0.000 5.500 0.000
-1 0 R
0 0 R

*END*
*REMARK* AP
HC49SSDLF-8.00MHz.stp/0/0/0/0/0/0
