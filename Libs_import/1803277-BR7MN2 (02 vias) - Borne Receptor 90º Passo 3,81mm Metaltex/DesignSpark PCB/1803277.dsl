SamacSys ECAD Model
13010083/1129843/2.50/2/2/Connector

DESIGNSPARK_INTERMEDIATE_ASCII

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "s180_h120"
		(holeDiam 1.2)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 1.800) (shapeHeight 1.800))
		(padShape (layerNumRef 16) (padShapeType Rect)  (shapeWidth 1.800) (shapeHeight 1.800))
	)
	(padStyleDef "c180_h120"
		(holeDiam 1.2)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.800) (shapeHeight 1.800))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.800) (shapeHeight 1.800))
	)
	(textStyleDef "Default"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 50 mils)
			(strokeWidth 5 mils)
		)
	)
	(patternDef "1803277" (originalName "1803277")
		(multiLayer
			(pad (padNum 1) (padStyleRef s180_h120) (pt 0.000, 0.000) (rotation 90))
			(pad (padNum 2) (padStyleRef c180_h120) (pt 3.810, 0.000) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 1.657, -3.400) (textStyleRef "Default") (isVisible True))
		)
		(layerContents (layerNumRef 28)
			(line (pt -2.6 1.2) (pt 6.41 1.2) (width 0.2))
		)
		(layerContents (layerNumRef 28)
			(line (pt 6.41 1.2) (pt 6.41 -8) (width 0.2))
		)
		(layerContents (layerNumRef 28)
			(line (pt 6.41 -8) (pt -2.6 -8) (width 0.2))
		)
		(layerContents (layerNumRef 28)
			(line (pt -2.6 -8) (pt -2.6 1.2) (width 0.2))
		)
		(layerContents (layerNumRef 30)
			(line (pt -4.095 2.2) (pt 7.41 2.2) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt 7.41 2.2) (pt 7.41 -9) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt 7.41 -9) (pt -4.095 -9) (width 0.05))
		)
		(layerContents (layerNumRef 30)
			(line (pt -4.095 -9) (pt -4.095 2.2) (width 0.05))
		)
		(layerContents (layerNumRef 18)
			(line (pt 6.41 1.2) (pt 6.41 -8) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 6.41 -8) (pt -2.6 -8) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2.6 -8) (pt -2.6 1.2) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.22 0) (pt -3.22 0) (width 0.050000000000000266))
		)
		(layerContents (layerNumRef 18)
			(arc (pt -3.195, 0) (radius 0.025) (startAngle 180) (sweepAngle 180.0) (width 0.050000000000000266))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.17 0) (pt -3.17 0) (width 0.050000000000000266))
		)
		(layerContents (layerNumRef 18)
			(arc (pt -3.195, 0) (radius 0.025) (startAngle .0) (sweepAngle 180.0) (width 0.050000000000000266))
		)
	)
	(symbolDef "1803277" (originalName "1803277")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -25 mils) (rotation 0]) (justify "Left") (textStyleRef "Default"))
		))
		(pin (pinNum 2) (pt 0 mils -100 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -125 mils) (rotation 0]) (justify "Left") (textStyleRef "Default"))
		))
		(line (pt 200 mils 100 mils) (pt 600 mils 100 mils) (width 6 mils))
		(line (pt 600 mils 100 mils) (pt 600 mils -200 mils) (width 6 mils))
		(line (pt 600 mils -200 mils) (pt 200 mils -200 mils) (width 6 mils))
		(line (pt 200 mils -200 mils) (pt 200 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 650 mils 300 mils) (justify Left) (isVisible True) (textStyleRef "Default"))

	)
	(compDef "1803277" (originalName "1803277") (compHeader (numPins 2) (numParts 1) (refDesPrefix J)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "1803277"))
		(attachedPattern (patternNum 1) (patternName "1803277")
			(numPads 2)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
			)
		)
		(attr "Mouser Part Number" "651-1803277")
		(attr "Mouser Price/Stock" "https://www.mouser.co.uk/ProductDetail/Phoenix-Contact/1803277?qs=8BCRtFWWXOQYeqQFCAtgBw%3D%3D")
		(attr "Manufacturer_Name" "Phoenix Contact")
		(attr "Manufacturer_Part_Number" "1803277")
		(attr "Description" "PCB header, nominal cross section: 1.5 mm?, color: green, nominal current: 8 A, rated voltage (III/2): 160 V, contact surface: Tin, type of contact: Male connector, Number of potentials: 2, Number of rows: 1, Number of positions per row: 2, number of connections: 2, product range: MC 1,5/..-G, pitch: 3.81 mm, mounting: Wave soldering, pin layout: Linear pinning, solder pin [P]: 3.4 mm, Stecksystem: MINI COMBICON, Locking: without, type of packaging: packed in cardboard")
		(attr "Datasheet Link" "http://www.phoenixcontact.com/gb/produkte/1803277")
		(attr "Height" "8.25 mm")
	)

)
