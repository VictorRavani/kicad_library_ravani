SamacSys ECAD Model
345121/1129843/2.50/2/2/Varistor

DESIGNSPARK_INTERMEDIATE_ASCII

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "s150_h100"
		(holeDiam 1)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 1.500) (shapeHeight 1.500))
		(padShape (layerNumRef 16) (padShapeType Rect)  (shapeWidth 1.500) (shapeHeight 1.500))
	)
	(padStyleDef "c150_h100"
		(holeDiam 1)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.500) (shapeHeight 1.500))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.500) (shapeHeight 1.500))
	)
	(textStyleDef "Default"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 50 mils)
			(strokeWidth 5 mils)
		)
	)
	(patternDef "B72210S0271K101" (originalName "B72210S0271K101")
		(multiLayer
			(pad (padNum 1) (padStyleRef s150_h100) (pt 0.000, 0.000) (rotation 90))
			(pad (padNum 2) (padStyleRef c150_h100) (pt 7.500, -1.800) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 3.750, -0.900) (textStyleRef "Default") (isVisible True))
		)
		(layerContents (layerNumRef 28)
			(line (pt -2.25 1.6) (pt 9.75 1.6) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt 9.75 1.6) (pt 9.75 -3.4) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt 9.75 -3.4) (pt -2.25 -3.4) (width 0.1))
		)
		(layerContents (layerNumRef 28)
			(line (pt -2.25 -3.4) (pt -2.25 1.6) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2.25 1.6) (pt 9.75 1.6) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 9.75 1.6) (pt 9.75 -3.4) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 9.75 -3.4) (pt -2.25 -3.4) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2.25 -3.4) (pt -2.25 1.6) (width 0.2))
		)
		(layerContents (layerNumRef 30)
			(line (pt -3.25 2.6) (pt 10.75 2.6) (width 0.1))
		)
		(layerContents (layerNumRef 30)
			(line (pt 10.75 2.6) (pt 10.75 -4.4) (width 0.1))
		)
		(layerContents (layerNumRef 30)
			(line (pt 10.75 -4.4) (pt -3.25 -4.4) (width 0.1))
		)
		(layerContents (layerNumRef 30)
			(line (pt -3.25 -4.4) (pt -3.25 2.6) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2.7 0) (pt -2.7 0) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(arc (pt -2.75, 0) (radius 0.05) (startAngle .0) (sweepAngle 180.0) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -2.8 0) (pt -2.8 0) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(arc (pt -2.75, 0) (radius 0.05) (startAngle 180) (sweepAngle 180.0) (width 0.1))
		)
	)
	(symbolDef "B72210S0271K101" (originalName "B72210S0271K101")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName false)) (pinName (text (pt 0 mils -35 mils) (rotation 0]) (justify "UpperLeft") (textStyleRef "Default"))
		))
		(pin (pinNum 2) (pt 700 mils 0 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName false)) (pinName (text (pt 700 mils -35 mils) (rotation 0]) (justify "UpperRight") (textStyleRef "Default"))
		))
		(line (pt 200 mils 50 mils) (pt 500 mils 50 mils) (width 6 mils))
		(line (pt 500 mils 50 mils) (pt 500 mils -50 mils) (width 6 mils))
		(line (pt 500 mils -50 mils) (pt 200 mils -50 mils) (width 6 mils))
		(line (pt 200 mils -50 mils) (pt 200 mils 50 mils) (width 6 mils))
		(line (pt 150 mils -100 mils) (pt 250 mils -100 mils) (width 6 mils))
		(line (pt 250 mils -100 mils) (pt 450 mils 100 mils) (width 6 mils))
		(line (pt 450 mils 100 mils) (pt 550 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 550 mils 250 mils) (justify Left) (isVisible True) (textStyleRef "Default"))

	)
	(compDef "B72210S0271K101" (originalName "B72210S0271K101") (compHeader (numPins 2) (numParts 1) (refDesPrefix RV)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Bidirectional))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "B72210S0271K101"))
		(attachedPattern (patternNum 1) (patternName "B72210S0271K101")
			(numPads 2)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
			)
		)
		(attr "Mouser Part Number" "871-B72210S271K101")
		(attr "Mouser Price/Stock" "https://www.mouser.co.uk/ProductDetail/EPCOS-TDK/B72210S0271K101?qs=dEfas%2FXlABJcVym03yTASg%3D%3D")
		(attr "Manufacturer_Name" "TDK")
		(attr "Manufacturer_Part_Number" "B72210S0271K101")
		(attr "Description" "Leaded Disk Varistors, Max. Operating Voltage [AC]=275Vrms, Max. Surge Current Imax(8/20?s,1time)=2500A")
		(attr "Datasheet Link" "https://product.tdk.com/system/files/dam/doc/product/protection/voltage/lead-disk-varistor/data_sheet/70/db/var/siov_leaded_standard.pdf")
		(attr "Height" "14.5 mm")
	)

)
