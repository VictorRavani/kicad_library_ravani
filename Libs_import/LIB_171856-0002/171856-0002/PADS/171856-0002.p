*PADS-LIBRARY-PART-TYPES-V9*

171856-0002 SHDR2W99P0X254_1X2_493X635X818P I CON 7 1 0 0 0
TIMESTAMP 2023.06.22.11.49.15
"Mouser Part Number" 538-171856-0002
"Mouser Price/Stock" https://www.mouser.co.uk/ProductDetail/Molex/171856-0002?qs=s7UCm7gO1bYKeEMrirBzyw%3D%3D
"Manufacturer_Name" Molex
"Manufacturer_Part_Number" 171856-0002
"Description" KK 254 RPC Pin Header friction lock 2way Molex KK 254 Series, Series Number 171856, 2.54mm Pitch 2 Way 1 Row Straight PCB Header, Through Hole
"Datasheet Link" https://www.digikey.com/product-detail/en/molex/1718560002/WM10153-ND/4423109
"Geometry.Height" 8.18mm
GATE 1 2 0
171856-0002
1 0 U 1
2 0 U 2

*END*
*REMARK* SamacSys ECAD Model
245001/1129843/2.50/2/3/Connector
