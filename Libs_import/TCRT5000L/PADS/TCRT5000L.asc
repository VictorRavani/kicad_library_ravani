!PADS-POWERPCB-V9.5-METRIC! DESIGN DATABASE ASCII FILE 1.0
*PARTDECAL*  ITEMS

TCRT5000L M 1.977 -1.528 6 7 4 0 2
CLOSED 5 0.2 0 27
-5.1  2.9  
5.1   2.9  
5.1   -2.9 
-5.1  -2.9 
-5.1  2.9  
OPEN 2 0.2 0 26
5.1   -2.9 
5.1   2.9  
OPEN 2 0.2 0 26
5.1   2.9  
1.5   2.9  
OPEN 2 0.2 0 26
5.1   -2.9 
1.5   -2.9 
OPEN 2 0.2 0 26
-5.1  2.9  
-1.5  2.9  
CIRCLE 2 0.2 0 26
-2.848 3.492
-2.732 3.492
VALUE -0.432 0.015 0 1 0.381 0.0381 N LEFT DOWN
Regular <Romansim Stroke Font>
Ref.Des.
VALUE -0.432 0.015 0 1 0.381 0.0381 N LEFT UP
Regular <Romansim Stroke Font>
Part Type
T-2.75 1.27  -2.75 1.27  1
T-2.75 -1.27 -2.75 -1.27 2
T2.75  -1.27 2.75  -1.27 3
T2.75  1.27  2.75  1.27  4
T0     1.9   0     1.9   5
T0     -1.9  0     -1.9  6
T-4.65 -2.45 -4.65 -2.45 7
PAD 0 3
-2 1.55 R   1
-1 1.55 R  
0  1.55 R  
PAD 5 3
-2 2.5 R   2.5
-1 2.5 R  
0  2.5 R  
PAD 6 3
-2 2.5 R   2.5
-1 2.5 R  
0  2.5 R  
PAD 7 3
-2 1   R   1
-1 1   R  
0  1   R
*END*     OF ASCII OUTPUT FILE
